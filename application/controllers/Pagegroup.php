<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pagegroup extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "wstm_page_group";
	}

	public function getlist()
	{
		$sql = "SELECT t1.id , t1.name , t1.maker_date, t1.status FROM " . $this->table . " AS t1 WHERE t1.id_language = ". $this->language ." ORDER BY t1.maker_date ASC";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$list = ($list != null) ? $list :  array();

		$message = $this->lang->line('success');

		$this->responsesuccess($message, $list);
	}

	public function getrow()
	{
		$id = $this->params['id'];

		$sql = "SELECT * FROM " . $this->table . " WHERE id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$message = $this->lang->line('success');

		$this->responsesuccess($message, $list);
	}

	public function process()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) && $this->params['id'] > 0 ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			$data['container'] = json_encode($data['container']);

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$data['id_language'] = $this->language;

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE name='" . $data['name'] . "' 

					AND id_language = ". $this->language ." ";

			if ($id > 0) {

				$sql .= " AND id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);

				} else {

					$this->db->where('id', $id);

					$this->db->where('id_language', $this->language);

					$is = $this->db->update($this->table, $data);
				}

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

			} else {

				$message = $this->lang->line('pagesGroupNameError');
			}
		} else {

			$message = $this->lang->line('failure');
		}
		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}

	public function remove()
	{
		$is = false;

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		if ($id > 0) {

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE  is_delete = 1 AND id=" . $id;

			if ($this->db->query($sql)->row_object()->count == 0) {

				$this->db->where('id', $id);

				$is = $this->db->delete($this->table);

				$message = $is == true ? $this->lang->line('success') : $this->lang->line('failure');
			} else {

				$message = $this->lang->line('isDelete');
			}
		} else {

			$message = $this->lang->line('failure');
		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
}
