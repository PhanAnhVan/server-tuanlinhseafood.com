<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends MY_Controller{
	
	public function getdashboard(){
		
		$table = array(
		    'pdtb_product',
			'wstm_content',
			'wstb_library',
			'wstm_slide'
		);
		$title=array(	
		    'Sản phẩm',
			'Tin tức',
			'Hình ảnh & Video',
			'Banner'
		);
		$class = array(
		    'fab fa-product-hunt',
			'fas fa-newspaper',
			'fas fa-photo-video',
			'fas fa-images'
		);
	
		// $color=array('bg-primary','bg-success',' bg-danger','bg-info','bg-info',' bg-danger','bg-success','bg-primary','bg-primary','bg-success',' bg-danger','bg-info',);
		$data=array();
		
		for ($i=0; $i < count($table); $i++) { 
		    
			$sqlon ="SELECT COUNT(id) AS count FROM ".$table[$i]." WHERE status='1' AND id_language = ".$this->language;
			
            $sqloff ="SELECT COUNT(id) AS count FROM ".$table[$i]." WHERE status='0' AND id_language = ".$this->language;
			
			$on = $this->db->query($sqlon)->row_object();
			
            $off = $this->db->query($sqloff)->row_object();
            
			//'color'=>$color[$i],
			array_push($data,array('class'=> $class[$i],'title'=>$title[$i] ,'on' => $on->count ,'off' => $off->count));
		}

		$this->responsesuccess($this->lang->line('success'), $data);
	}
	
	public function getUnRead(){
		$table = array(
			'wstb_contact',
		);
		$key=array(			
			'contact',
		);
		$data=array();
		for ($i=0; $i < count($table); $i++) { 
			$sql = "SELECT COUNT(id) AS count FROM ".$table[$i]." WHERE checked IS NULL OR checked = 0";
			$result = $this->db->query($sql)->row_object();
			array_push($data,array('key'=> $key[$i],'amount' => $result->count));
		}
		
		$this->responsesuccess($this->lang->line('success'), $data);
	}
}