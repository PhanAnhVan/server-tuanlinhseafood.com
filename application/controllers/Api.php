<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
	}
	public function logoutadmin()
	{

		@$this->session->sess_destroy();

		$this->responsesuccess(null, array());
	}
	public function checklogin()
	{

		$data = $this->getdata();
		$list = '';
		$skip = false;

		if ($data !== null) {

			$password = isset($data['password']) ? $data['password'] : '';
			$email = isset($data['email']) ? $data['email'] : '';
			$sql = "SELECT count(id) as count  FROM hrtb_user WHERE  status = 1 AND email='" . $email . "'  AND password ='" . $password . "'";

			$query = $this->db->query($sql);
			$skip = $this->db->query($sql)->row_object()->count == 1 ? true : false;

			if ($skip == true) {

				$sql = "SELECT * FROM hrtb_user WHERE email='" . $data['email'] . "' AND status=1 AND password ='" . $password . "'";

				$query = $this->db->query($sql);
				$list = $query->row_object();
				$this->session->set_userdata('user', true);
				$this->session->set_userdata('user_id', $list->id);
				$this->session->set_userdata('user_name', $list->name);
			}
		}

		$list = array('skip' => $skip , 'data'=> $list);
		echo json_encode($list);
	}
	//NOT DELETE - NOT UPDATE - TIENNV

	public function getmenu()
	{

		$position = $this->params['position'];

		$url = base_url() . 'public/pages/';

		$listid = array();

		$sql = "SELECT container FROM wstm_page_group WHERE position='" . $position . "' AND status = 1 AND id_language = ".$this->language;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		if ($list) {

			$listid = (strlen($list->container) > 1) ? json_decode($list->container) : array();
		}

		$result = array();

		if (count($listid) > 0) {

			$sql = "SELECT t1.id , t1.name, t1.type, t1.parent_id, t1.code,

			(CASE WHEN t1.type != 1 && t2.link != '' THEN CONCAT(language.code, '/', t2.link, '/', t1.link) 
			 
			WHEN t1.type != 1 && t2.link IS NULL THEN CONCAT(language.code, '/', t1.link) 
			 
			ElSE CONCAT(t1.link) END) AS link 
			
			FROM wstm_page AS t1
			
			LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id
			
			LEFT JOIN tb_language AS language ON t1.id_language = language.id
			
			WHERE t1.id IN (" . implode(',', $listid) . ") AND t1.status = 1 

			AND t1.id_language = ". $this->language ." ORDER BY t1.orders ASC";
			
			$query = $this->db->query($sql);
			
			$result = $query->result_object();
		}

		$this->responsesuccess($this->lang->line('success'), $result);
	}
	/**
     * 
     * Create
     * Name: NGUYỄN VĂN TIÊN   
     * Date : 05/01/2021
     * Note: API SETTINGS WEBSITE
     * ------------------
     * Edit
     * Name:  
     * Date : 
     * Note : 
     *
     **/
	public function company()
	{

		$url = base_url() . 'public/website/';

		$sql = "SELECT id, name, description, keywords, (CASE WHEN logo!='' THEN CONCAT('" . $url . "', logo) ELSE '' END) as logo, 
		
		(CASE WHEN logo_white!='' THEN CONCAT('" . $url . "', logo_white) ELSE '' END) as logo_white,
		
		(CASE WHEN shortcut!='' THEN CONCAT('" . $url . "', shortcut) ELSE '' END) as shortcut 
		
		FROM wstm_title WHERE status=1 AND id_language = ".$this->language;

		$query = $this->db->query($sql);

		$data = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	public function language()
	{
		$sql = "SELECT id, title, text_key, value 
		
		FROM wstm_setting WHERE status = 1 AND value != '' AND value != '/' AND id_language = ". $this->language ." ORDER BY id ASC";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$list = ($list != null) ? $list :  array();

		$message = $this->lang->line('success');

		$this->responsesuccess($message, $list);
	}
	
	public function mutiLanguage()
	{
		$url = base_url() . 'public/iconlang/';
		
		$sql = "SELECT id, code, name, defaults, 
		
		(CASE WHEN icon!='' THEN CONCAT('" . $url . "', icon) ELSE '' END) AS icon
		
		FROM tb_language WHERE status = 1";

		$query = $this->db->query($sql);
		$list = $query->result_object();
		$list = ($list != null) ? $list :  array();

		$message = $this->lang->line('success');

		$this->responsesuccess($message, $list);
	}
	/* GET SLIDES type - 1:  2:banner */
	public function getslide()
	{
		$url = base_url() . 'public/slides/';
		
		$type = isset($this->params['type']) ? $this->params['type'] : 1;
	
		 $sql = "SELECT id, name, link, title, description, orders, type, status , position, 
		
		(CASE WHEN images!='' THEN CONCAT('" . $url . "', images) ELSE '' END) AS images
		
		FROM wstm_slide WHERE status = 1 AND type = ".$type." AND id_language = ".$this->language." ORDER BY orders ASC ";
		
		$query = $this->db->query($sql);
		
		$list = $query->result_object();
		
		$list = ($list != null) ? $list :  array();
		
		$message = $this->lang->line('success');
		
		$this->responsesuccess($message, $list);
	}

	public function getAboutus()
	{
		$url = base_url() . 'public/pages/';

		$background = base_url() . 'public/pages/background/';
		
		$sql = "SELECT id, link, description, name, title,
		
		(CASE WHEN images!='' THEN CONCAT('" . $url . "',images) ELSE '' END) AS images, 

		(CASE WHEN background!='' THEN CONCAT('" . $background . "',background) ELSE '' END) AS background
		
		FROM wstm_page WHERE code = 'about' AND status = 1 AND id_language = ".$this->language;

		$query = $this->db->query($sql);
		
		$data = $query->row_object();
		
		$message = $this->lang->line('success');
		
		$this->responsesuccess($message, $data);
	}

	public function getProductsPin()
	{

		$limit = isset($this->params['limit']) ? $this->params['limit'] : 8;

		$url = base_url().'public/products/';		

		$sql = "SELECT id, name, title, description FROM wstm_page WHERE code = 'products' AND status = 1 AND id_language = ".$this->language;

		$data = $this->db->query($sql)->row_object();

		$sql = "SELECT id, name,

					(CASE WHEN images != '' THEN CONCAT('". $url ."', images) ELSE '' END) AS images

				FROM pdtb_product 

				WHERE page_id = ". $data->id ." AND status = 1 AND pin = 1 AND id_language = ". $this->language ." 

				ORDER BY maker_date DESC LIMIT ". $limit;

		$data->list = $this->db->query($sql)->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	public function getNewProducts()
 	{
 		
		$limit = isset($this->params['limit']) ? $this->params['limit'] : 3;

		$url = base_url().'public/products/';

		$sql = "SELECT id, name, link FROM wstm_page WHERE code = 'products' AND status = 1 AND id_language = ".$this->language;

		$data = $this->db->query($sql)->row_object();

		$sql = "SELECT id, name,

 					(CASE WHEN images != '' THEN CONCAT('". $url ."', images) ELSE '' END) AS images

					FROM pdtb_product 

					WHERE page_id = ". $data->id ." AND status = 1 AND id_language = ". $this->language ." 

					ORDER BY maker_date DESC LIMIT ". $limit;

		$data->list = $this->db->query($sql)->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);

 	}

 	public function getHotNews()
 	{
 		
		$limit = isset($this->params['limit']) ? $this->params['limit'] : 5;

		$url = base_url().'public/contents/';

		$sql = "SELECT id, name, link FROM wstm_page WHERE code = 'news' AND status = 1 AND id_language = ".$this->language;

		$data = $this->db->query($sql)->row_object();

		$sql = "SELECT t1.id, t1.name, t1.description, t1.link, t2.link AS parent_link, t3.link AS parent_links,

 					(CASE WHEN t1.images != '' THEN CONCAT('". $url ."', t1.images) ELSE '' END) AS images FROM wstm_content AS t1

 					LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id

 					LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id

					WHERE t1.status = 1 AND t1.id_language = ". $this->language ." AND t2.parent_id = t3.id AND t3.id = ". $data->id ."

					ORDER BY t1.maker_date DESC LIMIT ". $limit;

		$data->list = $this->db->query($sql)->result_object();

		if(!empty($data->list)) {

			$this->responsesuccess($this->lang->line('success'), $data);

		} else {

			$this->responsefailure($this->lang->line('failure'));

		}
 	}

	public function getChildrenPages()
	{

		$link = isset($this->params['link']) ? $this->params['link'] : '';

		$sql = "SELECT t1.id, t1.name, t1.link, t1.parent_id, t2.name AS parent_name, t2.link AS parent_link, t2.code AS parent_code FROM wstm_page AS t1

				LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id

				WHERE t1.status = 1 AND t1.link = '". $link . "' AND t1.id_language = ". $this->language;

		$data = $this->db->query($sql)->row_object();

		if($data->parent_id > 0) {

			$sql = "SELECT t1.id, t1.name, t1.link, t2.link AS parent_link FROM wstm_page AS t1

					LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id 

					WHERE t1.status = 1 AND t1.parent_id = ". $data->parent_id . " AND t1.id_language = ". $this->language ." ORDER BY t1.orders ASC";

			$data->list = $this->db->query($sql)->result_object();

			$this->responsesuccess($this->lang->line('success'), $data);

		} else {

			$sql = "SELECT t1.id, t1.name, t1.link, t2.link AS parent_link FROM wstm_page AS t1

					LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id 

					WHERE t1.status = 1 AND t1.parent_id = ". $data->id . " AND t1.id_language = ". $this->language ." ORDER BY t1.orders ASC";

			// $sql = "SELECT id, name, link FROM wstm_page WHERE status = 1 AND parent_id = ". $data->id . " AND id_language = ". $this->language ." ORDER BY orders ASC";

			$data->list = $this->db->query($sql)->result_object();

			$this->responsesuccess($this->lang->line('success'), $data);
			
		}
	}

	public function getVideoAboutPage()
	{

		$sql = "SELECT id, link, name, link_video AS video FROM wstm_page WHERE code = 'about' AND status = 1 AND id_language = ".$this->language;

		$data = $this->db->query($sql)->row_object();

		if(isset($data->link_video)) {

			$this->responsesuccess($this->lang->line('success'), $data);

		} else {

			$this->responsefailure($this->lang->line('failure'));

		}

	}

	public function getBanner()
	{

		$url = base_url() . 'public/slides/';
		
		$type = isset($this->params['type']) ? $this->params['type'] : 2;
	
	 	$sql = "SELECT id, name, link, 
		
		(CASE WHEN images != '' THEN CONCAT('" . $url . "', images) ELSE '' END) AS image
		
		FROM wstm_slide WHERE status = 1 AND type = ".$type." AND id_language = ".$this->language." LIMIT 1";

		$data = $this->db->query($sql)->row_object();

		if(isset($data->image)) {

			$this->responsesuccess($this->lang->line('success'), $data);

		} else {

			$this->responsefailure($this->lang->line('failure'));

		}
	}

	public function getPartnersPin()
	{

		$limit = isset($this->params['limit']) ? $this->params['limit'] : 7;

		$url = base_url() . 'public/partner/';

		$sql = "SELECT company, link, (CASE WHEN logo!='' THEN CONCAT('" . $url . "', logo) ELSE '' END) AS logo FROM tb_partner 
		
				WHERE status = 1 AND pin = 1 ORDER BY orders DESC LIMIT ". $limit;

		$data = $this->db->query($sql)->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	public function getProductList()
	{
		$id = (isset($this->params['id'])) ? $this->params['id'] : 0;	
			
		$url = base_url().'public/products/';		
			
		$sql = "SELECT t1.id, t1.name, t1.page_id, t1.views, t1.brand_id, t1.origin_id, t1.price_sale, t1.price_buy AS price,

		t5.name as brand_name, t6.name as origin_name, t1.views, t2.name AS page_name,

		t1.percent, (CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images,
		
		CONCAT(

			CASE WHEN t4.link !='' THEN CONCAT(t4.link,'/') ELSE '' END,

			CASE WHEN t3.link !='' THEN CONCAT(t3.link,'/') ELSE '' END,

			CASE WHEN t2.link !='' THEN CONCAT(t2.link,'/') ELSE '' END,

			t1.link

		) AS link
		
		FROM pdtb_product AS t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
	
		LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id

		LEFT JOIN wstm_page AS t4 ON t3.parent_id = t4.id

		LEFT JOIN prtm_brand AS t5 ON t1.brand_id = t5.id
				
		LEFT JOIN prtm_origin AS t6 ON t1.origin_id = t6.id

		WHERE t1.status = 1 ";

		if($id > 0){
		
			$sql .= " AND ( t1.page_id = ".$id." OR t2.parent_id = ". $id . " OR t3.parent_id = ". $id . " )";
		}

		$sql .= " ORDER BY t1.maker_date DESC";
					
		$query = $this->db->query($sql);
				
		$data = $query->result_object();	

		$this->responsesuccess($this->lang->line('success'), $data);		
								
	}

	public function getContentHome()
	{
		$url = base_url() . 'public/pages/';
				
		$limit = isset($this->params['limit']) ? $this->params['limit'] : 0;
		
		$sql = "SELECT id, link, name , title, description,
		
		(CASE WHEN images!='' THEN CONCAT('" . $url . "',images) ELSE '' END) AS images 
		
		FROM wstm_page WHERE code = 'news' AND status=1 AND id_language = ".$this->language;

		$query = $this->db->query($sql);
		
		$data = $query->row_object();
		
		if($data != null && $data->id > 0 ){

			$url = base_url() . 'public/contents/';
									
			$sql = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link, t1.detail, t1.maker_date , t1.page_id ,  
		
				(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,
				
				t2.name as page_name, t2.link AS parent_link , t3.link  as parent_links
				
				FROM wstm_content as t1 
				
				LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
				
				LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id
									
				WHERE t3.id = ".$data->id." AND t1.status=1 AND t1.id_language = ".$this->language." ORDER BY t1.maker_date DESC";
				
			if($limit > 0 ){
				
				$sql .= " LIMIT ".$limit."";
				
			}
			
			$query = $this->db->query($sql);
			
			$data->list = $query->result_object();
		}
		
		$this->responsesuccess($this->lang->line('success'), $data);
		
	}

	public function getContentList()
	{
		$id = (isset($this->params['id'])) ? $this->params['id'] : 0;

		$url = base_url() . 'public/contents/';
									
		$sql = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link, t1.detail, t1.maker_date , t1.page_id,

		t2.name as page_name, t2.link AS parent_link , t3.link  as parent_links, 

		(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images
				
		FROM wstm_content as t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
		
		LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id
							
		WHERE t1.status = 1 ";
				
		if($id > 0){
		
			$sql .= " AND ( t2.id = ".$id." OR t2.parent_id = ". $id . " )";
		}

		$sql .= " ORDER BY t1.maker_date DESC";

		$query = $this->db->query($sql);
		
		$data = $query->result_object();
		
		$this->responsesuccess($this->lang->line('success'), $data);		
	}

	public function getPageByLink()
	{	
		$url = base_url() . 'public/pages/';
		
		$link = isset($this->params['link']) ? $this->params['link'] : '';
				
		$sql = "SELECT t1.id , t1.type , t1.name, t1.title, t1.link, t1.detail,
		
		t2.name AS parent_name, t2.link AS parent_link, t2.code AS parent_code
		
		FROM wstm_page  AS t1
		
		LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id
		
		WHERE t1.status=1 AND t1.id_language = ".$this->language." AND t1.link ='".$link."'";

		$query = $this->db->query($sql);
				
		$data = $query->row_object();		
		
		$this->responsesuccess($this->lang->line('success'), $data);
	
	}
	
	public function getDetail()
	{
		$parent_link = isset($this->params['parent_link']) ? $this->params['parent_link']: '';

		$link = isset($this->params['link']) ? $this->params['link']: '';

		$sql = "SELECT type FROM wstm_page  WHERE status = 1 AND id_language = ".$this->language." AND link ='".$parent_link."'";

		$query = $this->db->query($sql);
				
		$data = $query->row_object();

		if($data != null && $data->type ==3){

			$sql = "UPDATE pdtb_product SET  views = views + 1  WHERE link = '" . $link . "'";

			$this->db->query($sql);

			$url = base_url().'public/products/';
								
			$sql = "SELECT t1.id, t1.code, t1.name, t1.page_id, t1.views,  t1.info, t1.description,  t1.detail,  t1.price_sale, t1.price_buy,

			t1.percent,  t2.name as parent_name,  t4.name as brand_name, t5.name as origin_name, t1.images, t1.listimages
			
			FROM pdtb_product AS t1 
			
			LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
						
			LEFT JOIN prtm_brand AS t4 ON t1.brand_id = t4.id
			
			LEFT JOIN prtm_origin AS t5 ON t1.origin_id = t5.id
			
			WHERE t1.status = 1 AND t1.link = '" . $link . "'";				
			
			$query = $this->db->query($sql);

			$list = $query->row_object();

			$sql = "SELECT t1.id, t1.name, t1.page_id, t1.views, t1.brand_id, t1.origin_id, t1.price_sale, t1.price_buy AS price,

			t1.views, t1.percent, (CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images,
			
			CONCAT(

				CASE WHEN t4.link !='' THEN CONCAT(t4.link,'/') ELSE '' END,

				CASE WHEN t3.link !='' THEN CONCAT(t3.link,'/') ELSE '' END,

				CASE WHEN t2.link !='' THEN CONCAT(t2.link,'/') ELSE '' END,

				t1.link

			) AS link
			
			FROM pdtb_product AS t1 
			
			LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
		
			LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id

			LEFT JOIN wstm_page AS t4 ON t3.parent_id = t4.id		

			WHERE t1.page_id = ".$list->page_id." AND t1.id != ".$list->id." ORDER BY t1.maker_date DESC LIMIT 10";
			
			$query = $this->db->query($sql);		

			$list->related = $query->result_object();

			$list->type_page = $data->type;

			$this->responsesuccess($this->lang->line('success'), $list);

		}

		if($data != null && $data->type == 4){

			$sql = "UPDATE wstm_content SET  views = views + 1  WHERE link = '" . $link . "'";

			$this->db->query($sql);

			$url = base_url() . 'public/contents/';

			$sql = "SELECT t1.id, t1.name, t1.description, t1.views, t1.link,
		
			t2.name as parent_name, t1.page_id, t1.detail, t1.maker_date, t1.author,
			
			(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images 
					
			FROM wstm_content as t1 
			
			LEFT JOIN wstm_page  AS t2 ON t1.page_id = t2.id 		
			
			WHERE t1.link='" . $link . "' AND t1.status=1";

			$query = $this->db->query($sql);

			$list = $query->row_object();

			$sql = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link,  t1.maker_date , t1.page_id , 
			
			(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,	
			
			t2.name as parent_name, t2.link AS parent_link , t3.link as parent_links
			
			FROM wstm_content as t1 		
			
			LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id		
			
			LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id		
			
			WHERE  t1.status = 1 AND t1.page_id = ".$list->page_id." AND t1.id != ".$list->id." 
			
			ORDER BY t1.maker_date DESC LIMIT 6";		
			
			$query = $this->db->query($sql);		

			$list->related = $query->result_object();

			$list->type_page = $data->type;

			$this->responsesuccess($this->lang->line('success'), $list);
				
		}

		
	}

	public function addContact() 
    {

        $data = $this->getdata();
        
        $is = false;

        if (!empty($data)) {

            $data['maker_date'] = date('Y-m-d H:i:s');

            $is = $this->db->insert('wstb_contact', $data);
        }
        if ($is == true) {
            $message = '';
            $message .= '----------------------------------------------';
            $message .= "<h3>Thông tin </h3>";
            $message .= "<p>Họ và tên: " . $data['name'] . "</p>";
            $message .= "<p>Số điện thoại: " . $data['phone'] . "</p>";
            $message .= "<p>Email: " . $data['email'] . "</p>";
            $message .= "<p>Nội dung: " . $data['message'] . "</p>";
            $subject = $data['subject'];
            $mail = $data['email'];
            $html = 'Cảm ơn bạn đã quan tâm đến chúng tôi. Chúng tôi sẽ liên hệ đến bạn sớm nhất.';
            $html .= "" . $message;
           	$this->sendmail($mail, $subject, $html); //send email khách hàng
            
            $detail = '';
            $detail .= "<p>Hệ thống vừa nhận được tin nhắn của khách hàng.</p>";
            $detail .= "" . $message;
           	$skip = $this->sendmail('hung@tuanlinhseafood.com', $subject, $detail);
        }
        if ($is == true) {

            $this->responsesuccess($this->lang->line('success'));

        } else {

            $this->responsefailure($this->lang->line('failure'));
        }
    }
	
	public function search(){

		$option = isset($this->params['keywords']) ? $this->params['keywords']:'';

		$option = urldecode($option);
		
		$url = base_url() . 'public/contents/';
      
      	$url2 = base_url() . 'public/products/';
      
      	$result = (object) array( "products" => "", "contents" => "" );
      	
      	$sqlNormal = "SELECT t1.id, t1.name, t1.link, t1.description,
		
					t2.name AS parent_name, t2.link AS parent_link,

					(CASE WHEN t1.images!='' THEN CONCAT('" . $url2 . "',t1.images) ELSE '' END) AS images 

					FROM pdtb_product as t1

					LEFT JOIN wstm_page as t2 ON t1.page_id = t2.id	
					
					WHERE t1.id_language =".$this->language." AND t1.status=1 AND t2.type = 3 AND ( t1.name like '%".$option."%' OR t1.description like '%".$option."%' OR t1.keywords like '%".$option."%') 
			        
			        ORDER BY (CASE WHEN t1.name LIKE '%".$option."%' THEN 1 
			    	
			        WHEN t1.description LIKE '%".$option."%'  THEN 2 ELSE 3 END) ";
      
      	$sqlDetail = "SELECT t1.id, t1.name, t1.link, t1.description,
		
					t2.name AS parent_name, t2.link AS parent_link,

					(CASE WHEN t1.images!='' THEN CONCAT('" . $url2 . "',t1.images) ELSE '' END) AS images 

					FROM pdtb_product as t1

					LEFT JOIN wstm_page as t2 ON t1.page_id = t2.id		
					
					WHERE t1.id_language =".$this->language." AND t1.status=1 AND t2.type = 3 AND ( ".$this->processKeySearch('t1.name',$option)." OR ".$this->processKeySearch('t1.description',$option)." OR ".$this->processKeySearch('t1.keywords',$option)." ) 
			        
			        ORDER BY (CASE WHEN t1.name LIKE '%".$option."%' THEN 1 
			    	
			        WHEN t1.description LIKE '%".$option."%'  THEN 2 ELSE 3 END) ";
	
		$query = $this->db->query($sqlNormal);

		$result->products = $query->result_object();
      
      	if(count($result->products) <= 0){

        	$query = $this->db->query($sqlDetail);

          	$list = $query->result_object();
        }

		$sqlNormal = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link, t1.detail, t1.maker_date , t1.page_id ,  
		
					(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,
						
					t2.name as page_name, t2.link AS parent_link , t3.link as parent_links, t3.name AS parent_name
						
					FROM wstm_content as t1 
						
					LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
						
					LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id		
					
					WHERE t1.id_language =".$this->language." AND t1.status=1 AND ( t1.name like '%".$option."%' OR t1.description like '%".$option."%' OR t1.keywords like '%".$option."%') 
			        
			        ORDER BY (CASE WHEN t1.name LIKE '%".$option."%' THEN 1 
			    	
			        WHEN t1.description LIKE '%".$option."%'  THEN 2 ELSE 3 END)";
      
      	$sqlDetail = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link, t1.detail, t1.maker_date , t1.page_id ,  
		
					(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,
						
					t2.name as page_name, t2.link AS parent_link , t3.link as parent_links, t3.name AS parent_name
						
					FROM wstm_content as t1 
						
					LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
						
					LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id		
					
					WHERE t1.id_language =".$this->language." AND t1.status=1 AND ( ".$this->processKeySearch('t1.name',$option)." OR ".$this->processKeySearch('t1.description',$option)." OR ".$this->processKeySearch('t1.keywords',$option)." ) 
			        
			        ORDER BY (CASE WHEN t1.name LIKE '%".$option."%' THEN 1 
			    	
			        WHEN t1.description LIKE '%".$option."%'  THEN 2 ELSE 3 END)";
	
		$query = $this->db->query($sqlNormal);

		$result->contents = $query->result_object();
      
      	if(count($result->contents) <= 0) {

        	$query = $this->db->query($sqlDetail);

          	$list = $query->result_object();
        }
      
        $this->responsesuccess($this->lang->line('success'), $result);
	}
    
	function processKeySearch($field,$keywords){
      
	    $result = $field.' like '."'%".$keywords."%'";
	      
	    $listKeywords = explode(' ',$keywords);
      
		for($i=0 ; $i < count($listKeywords); $i++){

			$result .= " OR ".$field.' like ';

			$result .= "'%".$listKeywords[$i]."%'";

		}

		return $result;
      
  	}
	
	function createLink($str)
	{
		$unicode = array(
			'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
			'd' => 'đ',
			'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
			'i' => 'í|ì|ỉ|ĩ|ị',
			'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
			'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
			'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
			'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
			'D' => 'Đ',
			'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
			'I' => 'Í|Ì|Ỉ|Ĩ|Ị',
			'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
			'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
			'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
		);

		foreach ($unicode as $nonUnicode => $uni) {
			$str = preg_replace("/($uni)/i", $nonUnicode, $str);
		}

		$str = $str;
		$str = strtolower(preg_replace('/[^a-zA-Z0-9\ ]/', '', $str));
		$str = preg_replace('/\s\s+/', ' ', trim($str));
		$str = str_replace(" ", "-", $str);

		return $str;
	}
	
	//LOGIN ADMIN
	public function adminLogin()
	{

		$data = $this->getdata();

		$is = false;

		$password = sha1($data['password']);

		$sql = "SELECT count(id) as count FROM hrtb_user WHERE email='" . $data['email'] . "' AND status = 1 AND password ='" . $password . "'";

		if ($this->db->query($sql)->row_object()->count == 1) {

			$sql = "SELECT * FROM hrtb_user WHERE  status = 1 AND email='" . $data['email'] . "'  AND password ='" . $password . "'";

			$query = $this->db->query($sql);

			$list = $query->row_object();

			$this->session->set_userdata('user', true);
			$this->session->set_userdata('user_id', $list->id);
			$this->session->set_userdata('user_name', $list->name);

			$is = true;
		}

		if ($is == true) {

			$this->responsesuccess($this->lang->line('loginSuccess'), $list);
		} else {

			$this->responsefailure($this->lang->line('failLogin'));
		}
	}
	
	public function processApi(){
	    
	    $path =  './public/contents/';
		
		$list = $this->getdata();
		
		$data = $list['data'];
		
		$token = $list['token'];
		
		$is = false;

		$message = $this->lang->line('failure');
		
		
	    if(!empty($data)){
	        
	        $sql ="SELECT COUNT(id) AS count FROM wstm_setting WHERE value='".$token."'";
	        
	        if ($this->db->query($sql)->row_object()->count == 1) {
	            
	            $countContentExit = 0;
	        
    	        $countContentScan = 0;
    	        
    	        $countContent =count($data);
	         
    	        for($i = 0 ; $i < count($data); $i++){
                
                    $data[$i]['maker_id'] = 1;
                    
                    $data[$i]['status'] = 1;
                    
                    $data[$i]['maker_date'] = date('Y-m-d H:i:s');
                    
                    $data[$i]['page_id'] = $data[$i]['group_id'] ;
                    
                    unset($data[$i]['group_id']);
                    unset($data[$i]['images_seo']);
                  
                    $data[$i]['images'] = $this->processImagesApi($data[$i]['images'], $path, $i);
                   
            		$data[$i]['link']= (array_key_exists('link' , $data[$i]) && strlen($data[$i]['link']) > 0) ? removesign($data[$i]['link']) : removesign($data[$i]['name']);
            	 
            		$sql ="SELECT COUNT(id) AS count FROM wstm_content WHERE link='".$data[$i]['link']."'";
             
            		if ($this->db->query($sql)->row_object()->count == 0) {
            
            		     $is = $this->db->insert('wstm_content', $data[$i]);
        		    
        		        $countContentScan = $is == true ?  $countContentScan + 1: $countContentScan;
            		    
            		}else{
            		    
            		    $countContentExit = $countContentExit + 1;
            		    
            		    continue;
            		   
            		}
                }
                
                if($countContent == $countContentExit){
                
                    $message =  'Tất cả tin đã được nạp trước đó';
                    
                }else if($countContent == $countContentScan){
                    
                    $message =  'Tất cả tin đã được nạp vào thành công';
                    
                }else {
                    
                    $message =  "Đã nạp được ".$countContentScan." tin và có ".$countContentExit." đã được nạp trước đó";;
                }
    	    } else {
	        
    	        $message = "Token bị sai";
    	    }
	    }else{
	        
	        $message = 'Không tồn tại dữ liệu';
	    }
		
		if ($is == true) {

			$this->responsesuccess($message,$data);
			
		} else {
			$this->responsefailure($message,$data);
		}
	}
	
	function processImagesApi($image, $path ,$i) {
	    
	    $extension = substr(explode( '/', $image )[1],0, 3);
	    
	    $newname = $i .'-' . time() . '.' . $extension;
	    
	    if(is_array(getimagesize($image))){
	        
	        @$file = file_get_contents($image);

			@$result = file_put_contents($path . $newname, $file);

	    }
	    
	   return $newname;
	 
    }
	
}
