<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Products extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		
		$this->table = "pdtb_product";
		
	}
	public function getlist()
	{

		$sql = "select t1.id, t1.name, t1.code, t1.images, t1.pin, t1.page_id, t1.price_sale, t1.price_buy, t1.orders, t1.views, t1.percent, t1.vat, t1.status, t1.maker_date, t2.name as namegroup		
		from " . $this->table . " AS t1 
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id WHERE t1.id_language = ".$this->language." AND t2.id_language = ".$this->language;

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess(null, $list);
	}
	public function getrow()
	{
		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$sql="SELECT t1.* FROM ".$this->table." AS t1  
		
		WHERE t1.id=".$id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess(null, $list);
	}
	public function remove(){
		
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');
		
		if($id > 0){

			$sql="select * from ".$this->table." where id=".$id;
			
			$list = $this->db->query($sql)->row_object();
									
			$this->db->where('id', $id);
			
			$is = $this->db->delete($this->table);
			
			if(isset($list->images) && strlen($list->images) > 4){
				
				@unlink('public/products/'.$list->images);
			}
			if(isset($list->listimages) && strlen($list->listimages) > 4){
				
				$listimages	= json_decode($list->listimages);	
						
				if (count($listimages) > 0) {
			
					foreach ($listimages as $k) {
												
						@unlink('public/products/'.$k);
						
					}
				}
			}
		}
				
		$message = ($is==true) ? $this->lang->line('success') : $this->lang->line('failure');
		
		if ($is == true) {

			$this->responsesuccess($message);
			
		} else {
			$this->responsefailure($message);
		}
	}
	public function process()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$data['id_language'] = $this->language;

			$data['code'] = time();

			$data['link'] = (array_key_exists('link', $data) && strlen($data['link']) > 1) ? removesign($data['link']) : removesign($data['name']);

			$sql = "select count(id) as count from " . $this->table . " where link='" . $data['link'] . "' AND id_language =".$this->language;

			if ($id > 0) {

				$sql .= " and id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);

					$id = $this->db->insert_id();
				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
				}

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
			} else {

				$message = $this->lang->line('checkExitname');
			}
		}


		if ($is) {

			$this->responsesuccess($message, $id);
		} else {

			$this->responsefailure($message);
		}
	}

	

	public function  updateImages()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			if (array_key_exists('images', $data)) {

				$data['images'] = $this->processimages($data['images']);
			}else {
				
				$data['images'] ='';
			}
			if (array_key_exists('listimages', $data)) {

				$data['listimages'] = $this->processimages($data['listimages']);
				
			}else {
				
				$data['listimages'] ='';
			}

			if ($id > 0) {

				$this->db->where('id', $id);

				$is = $this->db->update($this->table, $data);
			}
		}

		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is) {

			$this->responsesuccess($message, $id);
		} else {

			$this->responsefailure($message);
		}
	}

	public function updatePriceSeo()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			if (array_key_exists('price_sale', $data) && $data['price_sale'] > 0 && $data['price_sale'] != null) {

				$data['percent'] = 100 - ((($data['price_sale'] * 100) / $data['price_buy']));
			} else {

				$data['percent'] = 0;
			}

			if ($id > 0) {
   
				$this->db->where('id', $id);

				$is = $this->db->update($this->table, $data);
			}
		}
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}

	public function changeStatus(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$status = isset($this->params['status']) ? $this->params['status'] : 0;
		
		$is = false;
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('status' => $status));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is == true) {

			$this->responsesuccess($message);
			
		} else {
			$this->responsefailure($message);
		}
	}

	public function changePin(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$pin = isset($this->params['pin']) ? $this->params['pin'] : 0;
		
		$is = false;
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('pin' => $pin));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is == true) {

			$this->responsesuccess($message);
			
		} else {
			$this->responsefailure($message);
		}
	}
}
