<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Origin extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "prtm_origin";
	}
	public function getlist()
	{
		$sql = " SELECT id, name, status, maker_date FROM " . $this->table . " ORDER BY maker_date DESC";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$list = ($list != null) ? $list :  array();

		$this->responsesuccess($this->lang->line('success'), $list);
	}

	public function getrow()
	{

		$id = $this->params['id'];

		$sql = "select * from " . $this->table . " where id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}

	public function process()
	{
		$data = $this->getdata();

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');

		if ($data !== null) {

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$sql = "SELECT COUNT(id) AS count FROM " . $this->table . " WHERE name='" . $data['name'] . "'";

			if ($id > 0) {

				$sql .= " AND id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);
				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
				}

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
			} else {

				$message =  $this->lang->line('checkExitOrigin');
			}
		}
		if ($is == true) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}

	public function remove()
	{
		$id = $this->params['id'];

		$is = false;

		$message = $this->lang->line('failure');

		if ($id > 0) {

			$sql = "SELECT count(id) AS count FROM pdtb_product WHERE brand_name_id=" . $id;

			if ($this->db->query($sql)->row_object()->count == 0) {

				$this->db->where('id', $id);

				$is = $this->db->delete($this->table);
			} else {

				$message .= $this->lang->line('checkDeleteOrigin');
			}
		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
}
