<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / DASHBOARD
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
$route['get/dashboard/getlist'] = 'dashboard/getdashboard';
$route['get/dashboard/sitemap'] = 'dashboard/sitemap';
$route['get/dashboard/getUnRead'] = 'dashboard/getUnRead';
/** ================== END ==================**/
/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / PAGES
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
$route['get/pages/getlist'] = 'pages/getlist';
$route['get/pages/getlistsolution'] = 'pages/getlistsolution';
$route['get/pages/getrow'] = 'pages/getrow';
$route['get/pages/grouptype'] = 'pages/grouptype';
$route['set/pages/process'] = 'pages/process';
$route['set/pages/remove'] = 'pages/remove';
$route['set/pages/changestatus'] = 'pages/changestatus';
$route['set/pages/changePin'] = 'pages/changePin';
/** ================== END ==================**/

/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / PAGE GROUP / MENU
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
$route['get/pagesgroup/getlist'] = 'pagegroup/getlist';
$route['get/pagesgroup/getrow'] = 'pagegroup/getrow';
$route['set/pagesgroup/process'] = 'pagegroup/process';
$route['set/pagesgroup/remove'] = 'pagegroup/remove';
/** ================== END ==================**/


//contact
$route['get/contact/getlist'] = 'contact/getlist';
$route['get/contact/getrow'] = 'contact/getrow';
$route['set/contact/process'] = 'contact/process';
$route['set/contact/remove'] = 'contact/remove';
$route['set/contact/sendmail'] = 'contact/sendmailContact';

//content
$route['get/content/getlist'] = 'content/getlist';
$route['get/content/getrow'] = 'content/getrow';
$route['set/content/process'] = 'content/process';
$route['set/content/remove'] = 'content/remove';
$route['set/content/changepin'] = 'content/changepin';
$route['set/content/updateRelated'] = 'content/updateRelated';
$route['set/content/processApi'] = 'api/processApi';


// customer 
$route['get/customer/getlist'] = 'customer/getlist';
$route['get/customer/getrow'] = 'customer/getrow';
$route['set/customer/process'] = 'customer/process';
$route['set/customer/remove'] = 'customer/remove';
$route['set/customer/changetype'] = 'customer/changetype';

//router Image 
$route['get/library/getlist']='library/getlist';
$route['get/library/getrow']='library/getrow';
$route['set/library/process']='library/process';
$route['set/library/remove']='library/remove';



// personnel 
$route['get/user/getlist'] = 'user/getlist';
$route['get/user/getrow'] = 'user/getrow';
$route['set/user/process'] = 'user/process';
$route['set/user/remove'] = 'user/remove';
$route['set/user/changepassword'] = 'user/changepassword';
$route['set/user/changeToken'] = 'user/changeToken';


//settings
$route['get/settings/emailgetrow'] = 'settings/emailgetrow';
$route['set/settings/email'] = 'settings/email';
$route['get/settings/getorther'] = 'settings/getorther';
$route['set/settings/setorther'] = 'settings/setorther';
// $route['get/settings/settingAdmin'] = 'settings/settingAdmin';

$route['get/title/getrowcompany'] = 'title/getrowcompany';
$route['set/title/company'] = 'title/company';
$route['get/slide/getlist'] = 'slide/getlist';
$route['get/slide/getrow'] = 'slide/getrow';
$route['set/slide/process'] = 'slide/
';
$route['set/slide/remove'] = 'slide/remove';

/**
*
* Create
* Name: PHAN ANH VAN
* Date: 24/09/2021
* Note: SET PARTNER
* ---------------------
* Edit
* Name:
* Date:
* Note:
*
**/

$route['get/partner/getlist'] = 'partner/getlist';

$route['get/partner/getrow'] = 'partner/getrow';

$route['set/partner/process'] = 'partner/process';

$route['set/partner/remove'] = 'partner/remove';


$route['set/partner/changepin'] = 'partner/changepin';



/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / PRODUCTS, BRAND AND ORIGIN
 * ------------------
 * Edit
 * Name:   
 * Date :
 * Note : 
 *
 **/
// products
$route['get/products/getlist']='products/getlist';
$route['get/products/getrow']='products/getrow';
$route['set/products/process']='products/process';
$route['set/products/remove']='products/remove';
$route['set/products/updateImages']='products/updateImages';
$route['set/products/update']='products/updatePriceSeo';
$route['set/products/changestatus']='products/changeStatus';
$route['set/products/changepin']='products/changePin';

// brand
$route['get/brand/getlist'] = 'brand/getlist';
$route['get/brand/getrow'] = 'brand/getrow';
$route['set/brand/process'] = 'brand/process';
$route['set/brand/remove'] = 'brand/remove';

// origin
$route['get/origin/getlist'] = 'origin/getlist';
$route['get/origin/getrow'] = 'origin/getrow';
$route['set/origin/process'] = 'origin/process';
$route['set/origin/remove'] = 'origin/remove';

/** ================== END ROUTER BRAND ANH ORIGIN ==================**/


//Home
$route['api/getService']='api/getService';
$route['api/getAboutus']='api/getAboutus';
$route['api/getContentHome']='api/getContentHome';


//api getPage
$route['api/getPageByLink']='api/getPageByLink';
$route['api/getContentDetail']='api/getContentDetail';

/**
*
* Create
* Name: PHAN ANH VAN
* Date: 23/09/2021
* Note: GET PAGES BY ID
* ---------------------
* Edit
* Name:
* Date:
* Note:
*
**/
$route['api/pages/getChildrenPages']='api/getChildrenPages';

//add contact
$route['api/addcontact']='api/addContact';



//api addContact
$route['api/addContact']='api/addContact';


/* SEARCH */
$route['api/search'] = 'api/search';






$route['api/getmenu']='api/getmenu';
$route['api/company'] = 'api/company';

//api 
$route['api/logoutadmin'] = 'api/logoutadmin';
$route['api/checklogin']='api/checklogin';
$route['api/admin/login'] = "api/adminLogin";

// $route['api/home/getproductgroup']='api/getProductGroup';
// $route['api/getProductList']='api/getProductHome';

$route['api/getProductsPin']='api/getProductsPin';
$route['api/pages/getNewProducts']='api/getNewProducts';
$route['api/pages/getHotNews']='api/getHotNews';
$route['api/pages/getVideoAboutPage']='api/getVideoAboutPage';
$route['api/pages/getPartnersPin']='api/getPartnersPin';
$route['api/pages/getBanner']='api/getBanner';

$route['api/page/products']='api/getProductList';
$route['api/page/contents']='api/getContentList';

$route['api/page/productCategory']='api/productCategory';

//

/**
*
* Create
* Name: NGUYỄN VĂN TIÊN
* Date : 05/01/2021
* Note: API SETTINGS WEBSITE
* ------------------
* Edit
* Name:
* Date :
* Note :
*
**/
//Api language
$route['api/mutiLanguage']='api/mutiLanguage';
$route['api/language']='api/language';

$route['api/contact/add'] = 'api/addContact';
$route['api/home/slide'] = 'api/getSlide';
$route['api/pages/detail'] = 'api/getDetail';
$route['api/setting/language'] = 'api/setting';
$route['api/getmenu'] = 'api/getmenu';
$route['api/company'] = 'api/company';
$route['api/login/check'] = 'api/checklogin';
$route['api/login/admin'] = "api/loginAdmin";
$route['api/logout/admin'] = "api/logoutAdmin";
$route['(.*)'] = $route['default_controller'];
$route['translate_uri_dashes'] = FALSE;
/**================== END ==================**/